﻿"""Программу можно вызвать с ключами 'help' - вызов help, 's' - загрузка
сохраненной игры, 'f' - открытие редактора фигур, без ключа - начало игры.
"""
import sys
import os
import pygame
import pickle
import random
import numpy as np
import drawing
import Buttons
from pygame import K_DOWN
from pygame import MOUSEBUTTONDOWN
from enum import Enum


class Object(Enum):
    wall = 'wall'
    floor = 'floor'
    blocks = 'blocks'


class MagicPoint:
    """Класс, представляющиq фигуру 'магическая точка'."""
    def __init__(self):
        self.class_name = 'MagicPoint'
        self.cur_offset = 0
        self.cur_shape = [[[1, 1],
                           [1, 1]]]
        self.color = (205, 92, 92)
    def get_cur_phase(self):
        return self.cur_shape[self.cur_offset]
    def rotate(self):
        self.cur_offset += 1
        if self.cur_offset == len(self.cur_shape):
            self.cur_offset = 0
    def reverse_rotate(self):
        self.cur_offset -= 1
        if self.cur_offset == -1:
            self.cur_offset = len(self.cur_shape) - 1


def figures_loading():
    try:
        with open('figures.pickle', 'rb') as f:
            shapes = pickle.load(f)
    except FileNotFoundError:
        raise FileNotFoundError("Файл с фигурами не найден.")
    return shapes


class Figure:
    """Класс, представляющий фигуру."""
    shapes = figures_loading()
    colors = [(32, 178, 170), (105, 99, 92), (67, 205, 128), (255, 185, 15)]
    cur_shape = []
    def __init__(self, shape_number):
        self.class_name = 'Figure'
        self.cur_shape = self.shapes[shape_number]
        self.cur_offset = 0
        colors_number = random.randint(0, len(self.colors) - 1)
        self.color = self.colors[colors_number]

    def rotate(self):
        self.cur_offset += 1
        if self.cur_offset == len(self.cur_shape):
            self.cur_offset = 0
    def reverse_rotate(self):
        self.cur_offset -= 1
        if self.cur_offset == -1:
            self.cur_offset = len(self.cur_shape) - 1
    def get_cur_phase(self):
        return self.cur_shape[self.cur_offset]


class Field:
    """Класс, представляющий поле в данный момент."""
    total_rows = 25
    total_cols = 15
    field_array = [[[0] for i in range(15)] for i in range(25)]
    moving_figure = 0
    points = 0
    def set_figure(self):
        """Появление новой фигуры на поле."""
        start_x = 6
        start_y = 0
        init_int = random.randint(0, len(Figure.shapes))
        if self.moving_figure == 0:
            if init_int == len(Figure.shapes) :
                self.moving_figure = MagicPoint()
            else:
                self.moving_figure = Figure(init_int)
        else:
            self.moving_figure = self.next_figure
        init_int = random.randint(0, len(Figure.shapes))
        if init_int == len(Figure.shapes) :
            self.next_figure = MagicPoint()
        else:
            self.next_figure = Figure(init_int)
        self.figureX = start_x
        self.figureY = start_y


    def check_loss(self):
        """Проверка на проигрыш."""
        for i in range(self.total_cols):
            if self.field_array[0][i] == 1:
                return True
        return False


    def move_left(self):
        """Движение влево."""
        if self.check_collision(self.figureX - 1, self.figureY) is None:
            self.figureX -= 1


    def move_right(self):
        """Движение вправо."""
        if self.check_collision(self.figureX + 1, self.figureY) is None:
            self.figureX += 1


    def check_prop(self, newX, newY):
        """Если под фигурой в поле нет пустых клеток, возвращает True"""
        cur_phase = self.moving_figure.get_cur_phase()
        cols = len(cur_phase[0])
        for i in range(self.total_rows - newY):
            for j in range(cols):
                if self.field_array[newY + i][newX + j] == [0]:
                    return False
        return True


    def move_down(self):
        """Движение вниз."""
        collision_result = self.check_collision(self.figureX,
                                                self.figureY + 1)
        if collision_result is None:
            self.figureY += 1
        else:
            if self.moving_figure.class_name == 'MagicPoint':
                if collision_result is Object.floor or \
                   self.check_prop(self.figureX, self.figureY + 2):
                    self.fixate()
                    self.set_figure()
                else:
                    self.figureY += 1
            else:
                self.fixate()
                self.set_figure()
            if self.line_removal():
                True


    def rotate(self):
        """Поворот фигуры."""
        self.moving_figure.rotate()
        collision_result = self.check_collision(self.figureX, self.figureY)
        if self.moving_figure is MagicPoint:
            if collision_result is Object.floor:
                self.moving_figure.reverse_rotate()
        else:
            if collision_result is Object.blocks or \
               collision_result is Object.floor:
                self.moving_figure.reverse_rotate()
        if collision_result is Object.wall:
            self.wall_kick()


    def check_collision(self, newX, newY):
        """Проверка на столкновение фигуры со стеной,
        полом и другими блоками.
        """
        cur_phase = self.moving_figure.get_cur_phase()
        rows = len(cur_phase)
        cols = len(cur_phase[0])
        for i in range(rows):
            for j in range(cols):
                if cur_phase[i][j] == 1:
                    if(newX + j >= self.total_cols) or (newX + j < 0):
                        return Object.wall
                    if newY + i >= self.total_rows:
                        return Object.floor
                    if self.field_array[newY + i][newX + j] == 1:
                        return Object.blocks
        return None


    def wall_kick(self):
        """Обработка правильного поворота фигуры при столкновении со стеной."""
        if self.figureX < 0:
            self.figureX += 3
        else:
            if self.check_collision(self.figureX - 1, self.figureY) is Object.wall:
                self.figureX -= 2
            else:
                self.figureX -= 1


    def earning_points(self, criterion):
        """Начисление баллов."""
        reward_points = 0
        if criterion == 1:
            reward_points = 107
        elif criterion == 2:
            reward_points = 301
        elif criterion == 3:
            reward_points = 623
        elif criterion == 4:
            reward_points = 1000
        self.points += reward_points


    def line_removal(self):
        """Процесс удаления нижней линии."""
        ready_lines = 0
        for i in range(self.total_rows):
            removal = True
            for j in range(self.total_cols):
                if self.field_array[i][j] != 1:
                    removal = False
            if removal:
                ready_lines += 1
                for k in range(i, 0, -1):
                    for j in range(self.total_cols):
                        if k != 0:
                            self.field_array[k][j] = self.field_array[k - 1][j]
                        else:
                            self.field_array[k][j] = 0
        self.earning_points(ready_lines)
        return removal 


    def fixate(self):
        """Фиксирование приземленной фигуры в массиве поля заполнением
        соответствующих координат единицами.
        """
        cur_phase = self.moving_figure.get_cur_phase()
        rows = len(cur_phase)
        cols = len(cur_phase[0])
        for i in range(rows):
            for j in range(cols):
                if cur_phase[i][j] == 1:
                    self.field_array[self.figureY + i][self.figureX + j] = \
                    cur_phase[i][j]


def game(preservation):
    """Процесс игры, обработка нажатия клавиш."""
    game_field = Field()
    pygame.init()
    pygame.display.set_caption("Tetris")
    random.seed()
    game_field.set_figure()
    game_field.set_figure()
    prev_tick = pygame.time.get_ticks()
    game_over_label = True
    game_quit = False
    draw = drawing.Drawing()
    draw.screen_creation()
    if preservation is True:
        loading(game_field)
    while not game_quit:
        draw.drawing(game_field.moving_figure, game_field.next_figure,
                        Field.field_array, game_field)
        if game_field.check_loss() and game_over_label:
            game_over_label = False
        if not game_field.check_loss():
            cur_tick = pygame.time.get_ticks()
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        game_quit = True
                    elif event.key == pygame.K_LEFT:
                        game_field.move_left()
                    elif event.key == pygame.K_RIGHT:
                        game_field.move_right()
                    elif event.key == pygame.K_SPACE:
                        game_field.rotate()
                    elif event.key == pygame.K_s:
                        save(game_field)
                    elif event.key == pygame.K_p:
                        start = False
                        while not start:
                            events1 = pygame.event.get()
                            for event1 in events1:
                                if event1.type == pygame.KEYDOWN:
                                    if event1.key == pygame.K_p:
                                        start = True
                                    elif event1.key == pygame.K_ESCAPE:
                                        game_quit = True
                                        start = True
                            if start:
                                    break
            pressed_keys = pygame.key.get_pressed()
            if pressed_keys[K_DOWN]:
                down_delay = 50
            else:
                down_delay = 500
            pygame.event.pump()
            if cur_tick - prev_tick >= down_delay:
                prev_tick = cur_tick
                game_field.move_down()
            pygame.time.wait(30)
        else:
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        game_quit = True
    pygame.quit()


def save(game_field):
    """Сохранение игры."""
    data = {
            'field': Field.field_array,
            'moving_figure': game_field.moving_figure,
            'next_figure': game_field.next_figure,
            'points': Field.points
            }
    with open('save.pickle', 'wb') as f:
        pickle.dump(data, f)


def loading(game_field):
    """Загрузка сохраненных данных."""
    try:
        with open('save.pickle', 'rb') as f:
            data = pickle.load(f)
    except FileNotFoundError:
        raise FileNotFoundError("Файл с фигурами не найден.")
    Field.field_array = data['field']
    game_field.moving_figure = data['moving_figure']
    game_field.next_figure = data['next_figure']
    Field.points = data['points']


def figures_creation(drawing_panel):
    """Создание новой фигуры и ее положений при повороте."""
    new_figure = np.zeros((4, 4))
    for i in range(drawing_panel.shape[0]):
        for j in range(drawing_panel.shape[1]):
            if drawing_panel[j][i] == '*':
                new_figure[i][j] = 1
    figures_phases = np.empty((4, 4, 4), dtype = np.int8)
    figures_phases[0] = new_figure
    for i in range(figures_phases.shape[0] - 1):
        new_figure = np.rot90(new_figure)
        figures_phases[i + 1] = new_figure
    return figures_phases


def panel_buttons_pressing(drawing_panel, panel_buttons, panel_size):
    """Обработка нажатия клавиш панели рисования."""
    for i in range(panel_size):
        for j in range(panel_size):
            if panel_buttons[i][j].pressed(pygame.mouse.get_pos()):
                if drawing_panel[i][j] != '*':
                    drawing_panel[i][j] = '*'
                else:
                    drawing_panel[i][j] = ' '
    return drawing_panel


def del_buttons_pressing(del_buttons, figures):
    """Удаление элементов из списка фигур."""
    for i in range(len(del_buttons)):
        if  del_buttons[i].pressed(pygame.mouse.get_pos()):
            del(figures[i])
            with open('figures.pickle', 'wb') as f:
                pickle.dump(figures, f)


def ok_button_pressing(drawing_panel, panel_size, figures):
    """Добавление нового элемента в список фигур."""
    is_empty = True
    for i in range(panel_size):
        for j in range(panel_size):
            if drawing_panel[i][j] == '*':
                is_empty = False
    if not is_empty:
        figures_phases = figures_creation(drawing_panel)
        figures.append(figures_phases)
        with open('figures.pickle', 'wb') as f: 
            pickle.dump(figures, f)


def editor_figures():
    """Создание редактора фигур, обработка нажатия кнопок на его панели."""
    panel_size = 4
    drawing_panel = np.array([[' '] * panel_size for i in range(panel_size)])
    pygame.init()
    pygame.display.set_caption("РЕДАКТОР ФИГУР")
    prev_tick = pygame.time.get_ticks()
    game_quit = False
    draw = drawing.Drawing()
    draw.screen_figures_creation()
    try:
        with open('figures.pickle', 'rb') as f:
            figures = pickle.load(f)
    except FileNotFoundError:
        raise FileNotFoundError("Файл с фигурами не найден.")
    while not game_quit:
        del_buttons = draw.buttons_create(figures, drawing_panel)[0]
        panel_buttons = draw.buttons_create(figures, drawing_panel)[1]
        ok_button = draw.buttons_create(figures, drawing_panel)[2]
        draw.loading_figures(drawing_panel)
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    game_quit = True
            elif event.type == MOUSEBUTTONDOWN:
                drawing_panel = panel_buttons_pressing(drawing_panel, panel_buttons, panel_size)
                del_buttons_pressing(del_buttons, figures)
                if ok_button.pressed(pygame.mouse.get_pos()):
                    ok_button_pressing(drawing_panel, panel_size, figures)
    pygame.quit()


def main():
    help = 'Для движения фигуры по полю используйте клавиши стрелок, ' + \
           'для вращения нажмите пробел. Чтобы поставить игру на паузу, ' + \
           'нажмите клавишу "p", сохранить игру - "s", выйти из игры - ' + \
           '"Esc". Чтобы загрузить сохраненную игру, запустите ее с ' + \
           'параметром "s". Чтобы открыть редактор фигур, запустите ' + \
           'игру с параметром "f".'
    preservation = False
    if len(sys.argv) > 1:
        if sys.argv[1] == "help":
            print(help)
        elif sys.argv[1] == 's':
            preservation = True
            game(preservation)
        elif sys.argv[1] == 'f':
            editor_figures()
    else:
        game(preservation)


if __name__ == "__main__":
    main()
