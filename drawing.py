﻿import pygame
import pickle
import Buttons
import numpy as np
from pygame import Surface
from pygame.draw import line, rect
from pygame.font import SysFont


class Drawing:
    total_rows = 25
    total_cols = 15
    cell_size = 20
    screen = 0
    def screen_creation(self):
        """Создание экрана игры."""
        panel_width =  self.cell_size * 8
        screen_width = self.total_cols * self.cell_size + panel_width
        screen_height = self.total_rows * self.cell_size
        screen_size = screen_width, screen_height
        self.screen = pygame.display.set_mode(screen_size)

    def screen_figures_creation(self):
        """Создание экрана редактора фигур."""
        screen_width = 800
        screen_height = 500
        screen_size = screen_width, screen_height
        self.screen = pygame.display.set_mode(screen_size)

    def buttons_create(self, figures, drawing_panel):
        """Создание кнопок на экране редактора фигур."""
        del_buttons = []
        for i in range(len(figures)):
            add_button = Buttons.Button(self.screen, (255, 255, 255), i * 100 + 13, 3,
                                        50, 30, 'del', (0, 0, 0))
            del_buttons.append(add_button)
        panel_x = 30
        panel_y = 200
        panel_size = 4
        panel_buttons = [[0] * panel_size for i in range(panel_size)]
        for x in range(panel_size):
            for y in range(panel_size):
                add_button = Buttons.Button(self.screen, (255, 255, 255),
                                            panel_x + x * 30, panel_y + y * 30,
                                            30, 30, drawing_panel[x][y], (0, 0, 0))
                panel_buttons[x][y] = add_button
        ok_button = Buttons.Button(self.screen, (255, 255, 255), 180, 200, 50, 30,
                                   'OK', (0, 0, 0))
        pygame.display.flip()
        return (del_buttons, panel_buttons, ok_button)

    def loading_figures(self, panel):
        """Добавление на экран редактора тех фигур, которые сейчас есть в игре."""
        figure_image = Surface((self.cell_size, self.cell_size))
        self.screen.fill((102, 205, 170))
        figure_image.fill((205, 92, 92))
        rect(figure_image, (100, 92, 92), (0, 0, self.cell_size - 1, self.cell_size - 1), 1)
        with open('figures.pickle', 'rb') as f:
            figures = pickle.load(f)
        height = 3
        width = 5
        for i in range(len(figures)):
            rect(self.screen, (100, 92, 92), (i * width * self.cell_size + \
                 self.cell_size, 3 * self.cell_size, self.cell_size * \
                 4, self.cell_size * 4), 3)
            self.drawing_figures(len(figures[i][0]), len(figures[i][0]), figure_image,
                            figures[i][0], i*width + 1, height)

    def drawing(self, moving_figure, next_figure, field_array, game_field ):
        """Отрисовка игры."""
        next_figure_image = Surface((self.cell_size, self.cell_size))
        cell_image = Surface((self.cell_size, self.cell_size))
        figure_image = Surface((self.cell_size, self.cell_size))
        self.drawing_field(game_field)
        panel_x = 150 + self.cell_size * 8
        cell_image.fill((69, 139, 116))
        figure_image.fill(moving_figure.color)
        next_figure_image.fill(next_figure.color)
        rect(cell_image, (0, 0, 0), (0, 0, self.cell_size - 1, self.cell_size - 1), 1)
        rect(figure_image, (0, 100, 100), (0, 0, self.cell_size - 1,
             self.cell_size - 1), 1)
        rect(next_figure_image, (0, 100, 100),
            (0, 0, self.cell_size - 1, self.cell_size - 1), 1)
        self.drawing_figures(self.total_rows, self.total_cols, cell_image,
                        field_array)
        phase = moving_figure.get_cur_phase()
        self.drawing_figures(len(phase), len(phase[0]), figure_image, phase,
                        game_field.figureX, game_field.figureY)
        next_phase = next_figure.get_cur_phase()
        self.drawing_figures(len(next_phase), len(next_phase[0]), next_figure_image,
                        next_phase, 0, 0, panel_x, 30)
        if game_field.check_loss():
            game_over_drawing(game_field)
        pygame.display.flip()

    def game_over_drawing(self, game_field):
        """Отрисовка таблицы при проигрыше."""
        the_font = SysFont("monospace", 40, 5)
        the_font1 = SysFont("monospace", 30, 5)
        game_over_label = the_font.render("GAME OVER", 5, (165, 42, 42))
        score_label = the_font1.render("Your points: " + \
                                      str(game_field.points), 5,
                                      (165, 42, 42))
        self.screen.blit(game_over_label, (30, self.screen.get_height() / 2 - 10))
        self.screen.blit(score_label, (30, self.screen.get_height() / 2 + 30))


    def drawing_figures(self, phase_rows, phase_cols, figure, phase, x = 0, y = 0,
                       deltaX = 0, deltaY = 0):
        """Отрисовка фигур, находящихся на поле в данный момент."""
        for i in range(phase_rows):
            for j in range(phase_cols):
                blit_rect = (deltaX + (x + j) * figure.get_width(),
                deltaY + (y + i) * figure.get_height(),
                deltaX + (x + j + 1) * figure.get_width(),
                deltaY + (y + i + 1) * figure.get_height())
                if(phase[i][j] == 1):
                    self.screen.blit(figure, blit_rect)

    def drawing_field(self, game_field):
        """Отрисовка поля в данный момент."""
        self.screen.fill((102, 205, 170))
        line(self.screen, (69, 139, 116), (self.total_cols * self.cell_size, 0),
        (self.total_cols * self.cell_size, self.total_rows * self.cell_size), 5)
        the_font = SysFont("monospace", 18, 3)
        next_label = the_font.render("Next:", 5, (105, 139, 105))
        points_label = the_font.render("Points:" + str(game_field.points),
                                       5, (105, 139, 105))
        panel_x = 150 + self.cell_size * 8
        self.screen.blit(next_label, (panel_x, 10))
        self.screen.blit(points_label, (panel_x, 120))


if __name__ == "__main__":
    main()
