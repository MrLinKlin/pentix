import pentix
import drawing
import unittest
import numpy as np

class testSortings(unittest.TestCase):
    test_field = pentix.Field()
    test_field.set_figure()
    test_field1 = pentix.Field()
    test_field1.set_figure()

    def test_move_down(self):
        self.test_field.move_down()
        self.assertEqual([self.test_field.figureX, self.test_field.figureY],
                         [6, 1])

    def test_move_left(self):
        self.test_field.move_left()
        self.assertEqual([self.test_field.figureX, self.test_field.figureY],
                         [5, 1])

    def test_move_right(self):
        self.test_field.move_right()
        self.assertEqual([self.test_field.figureX, self.test_field.figureY],
                         [6, 1])

    def test_check_loss(self):
        self.test_field.field_array[0][0] = 1
        self.assertEqual(self.test_field.check_loss(), True)
        self.test_field.field_array[0][0] = 0

    def test_check_collision(self):
        self.assertEqual(self.test_field.check_collision(20, 0),
                         pentix.Object.wall)
        self.assertEqual(self.test_field.check_collision(0, 25),
                         pentix.Object.floor)
        self.assertEqual(self.test_field.check_collision(0, 0), None)

    def test_wall_kick(self):
        self.test_field.figureX = -1
        self.test_field.wall_kick()
        self.assertEqual(self.test_field.figureX, 2)

    def test_line_removal(self):
        for i in range(pentix.TOTAL_COLS):
            self.test_field.field_array[pentix.TOTAL_ROWS - 1][i] = 1
        self.test_field.line_removal()
        elements_sum = 0
        for i in range(pentix.TOTAL_COLS):
            elements_sum += self.test_field.field_array[pentix.TOTAL_ROWS - \
                                                        1][i][0]
        self.assertEqual(elements_sum, 0)

    def test_figures_creation(self):
        test_drawing_panel = np.array([['*', '*', ' ', ' '],
                                       ['*', ' ', ' ', ' '],
                                       ['*', ' ', ' ', ' '],
                                       [' ', ' ', ' ', ' ']])
        new_figures = np.array([
                      [[1, 1, 1, 0], [1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]],
                      [[0, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 0, 0]],
                      [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 1], [0, 1, 1, 1]],
                      [[0, 0, 1, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 0]]
                      ])
        self.assertEqual(pentix.figures_creation(test_drawing_panel)[0][0][3],
                         new_figures[0][0][3])

if __name__ == '__main__':
	unittest.main()
