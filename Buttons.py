import pygame


class Button:
    def __init__(self, surface, color, x, y, length, height, text, text_color):
        surface = self.draw_button(surface, color, length, height, x, y)
        surface = self.write_text(surface, text, text_color, length, height, x, y)
        self.rect = pygame.Rect(x,y, length, height)


    def write_text(self, surface, text, text_color, length, height, x, y):
        font_size = length//len(text) + 7
        myFont = pygame.font.SysFont("Calibri", font_size)
        myText = myFont.render(text, 1, text_color)
        surface.blit(myText, ((x+length / 2) - myText.get_width() / 2,
                    (y + height / 2) - myText.get_height() / 2 + 5))
        return surface


    def draw_button(self, surface, color, length, height, x, y):           
        pygame.draw.rect(surface, color, (x, y, length, height), 0)
        pygame.draw.rect(surface, (190, 190, 190), (x, y, length, height), 1)  
        return surface


    def pressed(self, mouse):
        return self.rect.topleft[0] < mouse[0] < self.rect.bottomright[0] and \
               self.rect.topleft[1] < mouse[1] < self.rect.bottomright[1]